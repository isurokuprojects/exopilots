
#include "DataTableUtils.h"

DEFINE_LOG_CATEGORY(LogLoadTable);

UDataTable* LoadTable(FString& PathName)
{
	// If there is no dot, add a dot and repeat the object name.
	int32 PackageDelimPos = INDEX_NONE;
	PathName.FindChar(TCHAR('.'), PackageDelimPos);
	if (PackageDelimPos == INDEX_NONE)
	{
		int32 ObjectNameStart = INDEX_NONE;
		PathName.FindLastChar(TCHAR('/'), ObjectNameStart);
		if (ObjectNameStart != INDEX_NONE)
		{
			const FString ObjectName = PathName.Mid(ObjectNameStart + 1);

			if (PathName[PathName.Len() - 1] == TCHAR('\''))
				PathName = PathName.Mid(0, PathName.Len() - 1);

			PathName += TCHAR('.');
			PathName += ObjectName;
		}
	}

	UClass* Class = UDataTable::StaticClass();
	Class->GetDefaultObject(); // force the CDO to be created if it hasn't already
	UDataTable* ObjectPtr = LoadObject<UDataTable>(NULL, *PathName);
	if (ObjectPtr)
	{
		ObjectPtr->AddToRoot();
	}

	ensureAlwaysMsgf(ObjectPtr != nullptr, TEXT("Can't load table! Path: %s"), *PathName);
	return ObjectPtr;
}