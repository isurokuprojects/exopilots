
#pragma once

#include "CoreMinimal.h"
#include "Engine.h"

DECLARE_LOG_CATEGORY_EXTERN(LogLoadTable, Log, All);

class UDataTable* LoadTable(FString& PathName);