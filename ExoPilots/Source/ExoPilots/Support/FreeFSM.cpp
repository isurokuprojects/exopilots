// Copyright (c) 2020 GFA Games. All rights reserved.


#include "FreeFSM.h"

UFreeFSMState::UFreeFSMState(const FName& InName): Name(InName)
{
}

UFreeFSMState::UFreeFSMState(): Name(NAME_None)
{
}

FString UFreeFSMState::ToString() const
{
	return FString::Printf(TEXT("%s"), *Name.ToString());
}

bool UFreeFSMState::OnExit(UFreeFSMState* InNextState, FFreeFSMSwitchData* InSwitchData)
{
	bActive = false;
	return true;
}

bool UFreeFSMState::OnEnter(UFreeFSMState* InPrevState, FFreeFSMSwitchData* InSwitchData)
{
	bActive = true;
	return true;
}

UFreeFSM::UFreeFSM(): CurrentState(nullptr)
{
}


bool UFreeFSM::Switch(UFreeFSMState* InNewState, FFreeFSMSwitchData* InSwitchData /*= nullptr*/)
{
	if(CurrentState != nullptr && !CurrentState->OnExit(InNewState, InSwitchData))
		return false;

	if (InNewState == nullptr || !InNewState->OnEnter(CurrentState, InSwitchData))
		return false;

	CurrentState = InNewState;

	return true;
}