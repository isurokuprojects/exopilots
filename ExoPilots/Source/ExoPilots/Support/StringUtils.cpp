// Copyright (c) 2020 GFA Games. All rights reserved.


#include "StringUtils.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Engine/GameInstance.h"

FString RoleToString(ENetRole Role)
{
	static FString Names[] =
	{
		FString("None"),
		FString("SimulatedProxy"),
		FString("AutonomousProxy"),
		FString("Authority"),
		FString("MAX"),
	};

	return Names[Role];
}

FString NetModeToString(ENetMode InNetMode)
{
	static FString Names[] =
	{
		FString("Standalone"),
		FString("DedicatedServer"),
		FString("ListenServer"),
		FString("Client"),
		FString("MAX"),
	};

	return Names[InNetMode];
}

FString ActorNetInfoToString(const AActor* InActor)
{
	if (InActor == nullptr)
		return FString();

	ENetMode NetMode = InActor->GetNetMode();

	uint32 GIID = 0;
	if (InActor->GetWorld() != nullptr && InActor->GetWorld()->GetGameInstance() != nullptr)
	{
		UGameInstance* GI = InActor->GetWorld()->GetGameInstance();
		GIID = GI->GetUniqueID();
	}

	ENetRole LR = InActor->GetLocalRole();
	ENetRole RR = InActor->GetRemoteRole();

	FString Res = FString::Printf(TEXT("%p: %s[%d] %s[%s]"), InActor, *NetModeToString(NetMode), GIID, *RoleToString(LR), *RoleToString(RR));
	return Res;
}
