// Copyright (c) 2020 GFA Games. All rights reserved.


#include "ActorUtils.h"
#include "GameFramework/Actor.h"

bool IsNeedRaiseRepEventManually(const AActor* InActor)
{
	if (InActor == nullptr)
		return false;

	const ENetMode NetMode = InActor->GetNetMode();

	if (NetMode == NM_Client || NetMode == NM_DedicatedServer)
	{
		// Clear Client Or Clear Server
		return false;
	}

	if (NetMode == NM_Standalone)
	{
		// Not networked.
		return true;
	}

	ENetRole LR = InActor->GetLocalRole();
	ENetRole RR = InActor->GetRemoteRole();

	if (NetMode == NM_ListenServer)
	{
		// Networked client in control.
		return RR == ROLE_SimulatedProxy;
	}

	return false;
}

bool IsActorOnPlayerComp(const AActor* InActor)
{
	if (InActor == nullptr)
		return false;

	const ENetMode NetMode = InActor->GetNetMode();

	if (NetMode == NM_Standalone)
	{
		// Not networked.
		return true;
	}

	if (NetMode == NM_DedicatedServer)
	{
		// Pure server.
		return false;
	}

	ENetRole LR = InActor->GetLocalRole();
	ENetRole RR = InActor->GetRemoteRole();

	if (NetMode == NM_Client)
	{
		// Or Networked client in control.
		return LR == ROLE_AutonomousProxy;
	}

	if (NetMode == NM_ListenServer)
	{
		// Networked client in control.
		return RR == ROLE_SimulatedProxy;
	}

	return false;
}
