// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "Engine/EngineBaseTypes.h"

/**
 * 
 */
FString RoleToString(ENetRole Role);
FString NetModeToString(ENetMode InNetMode);

FString ActorNetInfoToString(const AActor* InActor);
