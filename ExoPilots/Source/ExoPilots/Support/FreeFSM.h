// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FreeFSM.generated.h"

class FFreeFSMSwitchData
{
public:
	virtual ~FFreeFSMSwitchData() {}
};

UCLASS()
class EXOPILOTS_API UFreeFSMState : public UObject
{
	GENERATED_BODY()

public:
	UFreeFSMState();
	UFreeFSMState(const FName& InName);

	FString ToString() const;

	virtual bool OnExit(UFreeFSMState* InNextState, FFreeFSMSwitchData* InSwitchData);

	virtual bool OnEnter(UFreeFSMState* InPrevState, FFreeFSMSwitchData* InSwitchData);

	FORCEINLINE bool IsActive() const { return bActive; }

protected:

	UPROPERTY()
	FName Name;

	UPROPERTY()
	bool bActive;
};


UCLASS()
class EXOPILOTS_API UFreeFSM : public UObject
{
	GENERATED_BODY()

public:	
	UFreeFSM();

	bool Switch(UFreeFSMState* InNewState, FFreeFSMSwitchData* InSwitchData = nullptr);

	FORCEINLINE UFreeFSMState* GetCurrentState() const { return CurrentState; }

private:

	UPROPERTY()
	UFreeFSMState* CurrentState;
};


