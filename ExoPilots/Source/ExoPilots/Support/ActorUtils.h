// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"

/**
 * We have to call OnRep func on some class member (for example).
 * But if NetMode == NM_Standalone or NM_ListenServer (on hoster's app instance)
 * OnRep won't be called. And we must call ownself;
 */
bool IsNeedRaiseRepEventManually(const AActor* InActor);

/**
 * We can make UI on pure client (if it's our actor) or clien-server simultaneously: NM_Client or NM_Standalone and NM_ListenServer (we hoster)
 */
bool IsActorOnPlayerComp(const AActor* InActor);
