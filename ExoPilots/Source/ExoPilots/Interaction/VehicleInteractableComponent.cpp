// Fill out your copyright notice in the Description page of Project Settings.


#include "VehicleInteractableComponent.h"
#include "HumanPlayerInteractionComponent.h"
#include "../BaseEngineClasses/EPPlayerController.h"

void UVehicleInteractableComponent::StartHumanInteract(UHumanPlayerInteractionComponent* InInteractor)
{
	Super::StartHumanInteract(InInteractor);

	AEPPlayerController* PC = InInteractor->GetPlayerController();
	
	APawn* P = Cast<APawn>(GetOwner());
	ensureAlways(P);

	PC->Possess(P);
}