// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "InteractableComponent.generated.h"

class UHumanPlayerInteractionComponent;
/**
 * 
 */
UCLASS(ClassGroup = (Custom), BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class EXOPILOTS_API UInteractableComponent : public UBoxComponent
{
	GENERATED_BODY()
	
public:

	/** Max Angle between camera view and direction to object. For selection object. */
	UPROPERTY(EditAnywhere, Category = "Interaction Properties")
	float InteractionConusAngle;

	float GetFrontOfFaceValue(const FVector& InCamPosition, const FVector& InCamDirection);

	void SetIsSelected(bool InValue);

	virtual void StartHumanInteract(UHumanPlayerInteractionComponent* InInteractor);

protected:

	virtual void BeginPlay() override;

private:

	float _InteractionCosinus;
};
