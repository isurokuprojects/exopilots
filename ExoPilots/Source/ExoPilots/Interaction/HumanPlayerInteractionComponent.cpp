// Fill out your copyright notice in the Description page of Project Settings.


#include "HumanPlayerInteractionComponent.h"

#include "../Characters/EPCharacter.h"
#include "../BaseEngineClasses/EPPlayerController.h"
#include "VehicleInteractableComponent.h"

DEFINE_LOG_CATEGORY(LogHumanInteractor);

UHumanPlayerInteractionComponent::UHumanPlayerInteractionComponent(): UCapsuleComponent()
{
	bWantsInitializeComponent = true;
	PrimaryComponentTick.bCanEverTick = true;
}

void UHumanPlayerInteractionComponent::InitializeComponent()
{
	AEPCharacter* Char = GetPlayerCharacter();
	Char->OnPossessedHandlers.AddDynamic(this, &UHumanPlayerInteractionComponent::OnPossessed);
	Char->OnUnPossessedHandlers.AddDynamic(this, &UHumanPlayerInteractionComponent::OnUnPossessed);
}

void UHumanPlayerInteractionComponent::OnPossessed(AEPPlayerController* InPlayerController, bool InServerCall)
{
	UE_LOG(LogHumanInteractor, Log, TEXT("OnPossessed: PC_Role: %s; InServerCall %d"), *InPlayerController->GetRoleTextInfo(), InServerCall);

	if (IsNeedFindInteract())
	{
		UE_LOG(LogHumanInteractor, Log, TEXT("OnPossessed:IsCanFindIntersect true"));
		
		/*FindOverlapOnBeginPlay();

		OnComponentBeginOverlap.AddDynamic(this, &UHumanPlayerInteractionComponent::OnBeginOverlap);
		OnComponentEndOverlap.AddDynamic(this, &UHumanPlayerInteractionComponent::OnEndOverlap);*/
	}
	else
	{
		UE_LOG(LogHumanInteractor, Log, TEXT("OnPossessed:IsCanFindIntersect false"));
		SetCollisionEnabled(ECollisionEnabled::NoCollision);
		SetComponentTickEnabled(false);
	}
}

void UHumanPlayerInteractionComponent::OnUnPossessed()
{
	UE_LOG(LogHumanInteractor, Log, TEXT("OnUnPossessed"));
}

bool UHumanPlayerInteractionComponent::IsNeedFindInteract() const
{
	const ENetMode NetMode = GetNetMode();

	ACharacter* CharOwner = Cast<ACharacter>(GetOwner());
	ensureAlways(CharOwner);
	ENetRole LR = CharOwner->GetLocalRole();
	ENetRole RR = CharOwner->GetRemoteRole(); //for debug

	UGameInstance* GI = GetWorld()->GetGameInstance(); //for debug

	if (NetMode == NM_Standalone || NetMode == NM_Client)
	{
		// Not networked.
		// Or Networked client in control.
		return true;
	}

	if (NetMode == NM_ListenServer)
	{
		// Networked client in control.
		return LR == ROLE_AutonomousProxy;
	}

	return false;
}

AEPCharacter* UHumanPlayerInteractionComponent::GetPlayerCharacter() const
{
	AEPCharacter* CharOwner = Cast<AEPCharacter>(GetOwner());
	ensureAlways(CharOwner);
	return CharOwner;
}


AEPPlayerController* UHumanPlayerInteractionComponent::GetPlayerController() const
{
	ACharacter* CharOwner = Cast<ACharacter>(GetOwner());
	if (!CharOwner)
		return nullptr;

	return CharOwner->GetController<AEPPlayerController>();
}

void UHumanPlayerInteractionComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	SelectInteractableObject();
}

void UHumanPlayerInteractionComponent::SelectInteractableObject()
{
	UInteractableComponent* OldSelectObject = _SelectedObject;
	_SelectedObject = FindInteractableObjectForSelect();

	if (OldSelectObject != nullptr && _SelectedObject != OldSelectObject)
	{
		UE_LOG(LogHumanInteractor, Log, TEXT("SelectedUsableObject dissapeared!"));
		OldSelectObject->SetIsSelected(false);

		OnSelectedInteractableObjectHandlers.Broadcast(nullptr);
	}

	if (_SelectedObject != nullptr && _SelectedObject != OldSelectObject)
	{
		UE_LOG(LogHumanInteractor, Log, TEXT("New SelectedUsableObject: %s"), *_SelectedObject->GetName());
		_SelectedObject->SetIsSelected(true);

		OnSelectedInteractableObjectHandlers.Broadcast(_SelectedObject);
	}
}

UInteractableComponent* UHumanPlayerInteractionComponent::FindInteractableObjectForSelect()
{
	if(OverlappingComponents.Num() == 0)
		return nullptr;

	AEPPlayerController* PlayerController = GetPlayerController();
	if (!PlayerController)
		return nullptr;

	//character params
	const FVector CamPosition = PlayerController->PlayerCameraManager->GetCameraLocation();
	const FVector CamDirection = PlayerController->PlayerCameraManager->GetCameraRotation().Vector();

	UInteractableComponent* SelectedObject = nullptr;
	float SelectedConusCos = 0;

	for (const FOverlapInfo& OtherOverlap : OverlappingComponents)
	{
		UInteractableComponent* const UsableObject = Cast<UInteractableComponent>(OtherOverlap.OverlapInfo.Component.Get());
		if (UsableObject)
		{
			float DirCos = UsableObject->GetFrontOfFaceValue(CamPosition, CamDirection);

			if (FMath::IsNearlyZero(DirCos))
			{
				//UE_LOG(LogHumanInteractor, Log, TEXT("FindInteractableObjectForSelect: Usable Obj %s; Not front"), *SelectedObject->GetName());
				continue;
			}

			if (SelectedObject && DirCos < SelectedConusCos)
			{
				//UE_LOG(LogHumanInteractor, Log, TEXT("FindInteractableObjectForSelect: Usable Obj %s; Dir worse"), *SelectedObject->GetName());
				continue;
			}

			FString FailReason;
			if (!CheckInteractable(SelectedObject, FailReason))
			{
				/*UE_LOG(LogHumanInteractor, Log, TEXT("FindInteractableObjectForSelect: Usable Obj %s; FailReason: %s"),
					*SelectedObject->GetName(), *FailReason);*/
				continue;
			}

			SelectedConusCos = DirCos;
			SelectedObject = UsableObject;
		}
	}

	return SelectedObject;
}

bool UHumanPlayerInteractionComponent::CheckInteractable(UInteractableComponent* InInteractableObj, FString& OutFailReason)
{
	/*if (!UsableObject->IsInteractableExt(Char, FailReason))
	{
		UE_LOG(LogHumanInteractor, Log, TEXT("FindUsableObjectForSelect: Usable Obj %s; FailReason: %s"),
			*UsableObject->GetName(), *FailReason);
		continue;
	}*/
	return true;
}

void UHumanPlayerInteractionComponent::StartInteractOnClient()
{
	if (!_SelectedObject)
		return;

	Server_CallStartInteract(_SelectedObject);
}

void UHumanPlayerInteractionComponent::Server_CallStartInteract_Implementation(UInteractableComponent* InSelectedObj)
{
	if (!ensure(InSelectedObj))
		return;

	InSelectedObj->StartHumanInteract(this);
}