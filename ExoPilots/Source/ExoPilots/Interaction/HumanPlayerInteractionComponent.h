// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "HumanPlayerInteractionComponent.generated.h"

class AEPCharacter;
class AEPPlayerController;
class UInteractableComponent;

DECLARE_LOG_CATEGORY_EXTERN(LogHumanInteractor, Log, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSelectedInteractableObjectDelegate, UInteractableComponent*, InInteractableObject);

/**
 * 
 */
UCLASS(ClassGroup = (Custom), BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class EXOPILOTS_API UHumanPlayerInteractionComponent : public UCapsuleComponent
{
	GENERATED_BODY()

public:
	UHumanPlayerInteractionComponent();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FSelectedInteractableObjectDelegate OnSelectedInteractableObjectHandlers;

	void StartInteractOnClient();

	AEPCharacter* GetPlayerCharacter() const;
	AEPPlayerController* GetPlayerController() const;

protected:
	virtual void InitializeComponent() override;

private:

	bool IsNeedFindInteract() const;

	UFUNCTION()
	void OnPossessed(AEPPlayerController* InPlayerController, bool InServerCall);
	UFUNCTION()
	void OnUnPossessed();

	bool CheckInteractable(UInteractableComponent* InInteractableObj, FString& OutFailReason);

	UInteractableComponent* FindInteractableObjectForSelect();
	void SelectInteractableObject();

	UFUNCTION(Server, unreliable, Category = Interaction)
	void Server_CallStartInteract(UInteractableComponent* InSelectedObj);

	UPROPERTY()
	UInteractableComponent* _SelectedObject;
};
