// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "VehicleInteractableComponent.generated.h"

class UHumanPlayerInteractionComponent;

/**
 * 
 */
UCLASS()
class EXOPILOTS_API UVehicleInteractableComponent : public UInteractableComponent
{
	GENERATED_BODY()
	
public:
	virtual void StartHumanInteract(UHumanPlayerInteractionComponent* InInteractor) override;
};
