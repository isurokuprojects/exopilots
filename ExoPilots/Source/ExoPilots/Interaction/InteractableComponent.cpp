// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableComponent.h"
#include "HumanPlayerInteractionComponent.h"

void UInteractableComponent::BeginPlay()
{
	Super::BeginPlay();

	_InteractionCosinus = FMath::Cos(FMath::DegreesToRadians(InteractionConusAngle));
}

float UInteractableComponent::GetFrontOfFaceValue(const FVector& InCamPosition, const FVector& InCamDirection)
{
	const FVector ObjPosition = GetComponentLocation();

	FVector DirectionToObject = ObjPosition - InCamPosition;
	DirectionToObject.Normalize();

	float DirCos = FVector::DotProduct(InCamDirection, DirectionToObject);
	if (DirCos < _InteractionCosinus)
		return 0;
	return DirCos;
}

void UInteractableComponent::SetIsSelected(bool InValue)
{

}

void UInteractableComponent::StartHumanInteract(UHumanPlayerInteractionComponent* InInteractor)
{

}
