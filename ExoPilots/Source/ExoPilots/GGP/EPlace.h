// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "EPlace.generated.h"

UENUM()
enum EPlace
{
	EPlaceUndefined = 0,
	EPlaceLeft = 1,    // Left
	EPlaceRight = 2,    // Right
	EPlaceUp = 4,    // Up
	EPlaceDown = 8,    // Down
	EPlaceFront = 16,   // Front
	EPlaceBack = 32,   // Back
};