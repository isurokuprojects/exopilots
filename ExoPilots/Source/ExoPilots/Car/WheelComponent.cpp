// Fill out your copyright notice in the Description page of Project Settings.

#include "WheelComponent.h"
#include "CarComponent.h"

#include "../StaticData/ICarClentDatas.h"
#include "../Support/ActorUtils.h"
#include "../BaseEngineClasses/EPGameInstance.h"

#include <Engine.h>
#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY(LogWheelComponent);

// Sets default values for this component's properties
UWheelComponent::UWheelComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
}

void UWheelComponent::Init(UCarComponent* InCarHost, EPlace InPlace)
{
	_host = InCarHost;
	_Place = InPlace;
	//_SocketName = InSocketName;
	//_BaseTransform = InBaseTransform;
}

void UWheelComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	// Here we list the variables we want to replicate + a condition if wanted
	DOREPLIFETIME(UWheelComponent, _host);
	DOREPLIFETIME(UWheelComponent, _SocketName);
	DOREPLIFETIME(UWheelComponent, _BaseTransform);
}

// Called when the game starts
void UWheelComponent::BeginPlay()
{
	Super::BeginPlay();

	/*if (!GetOwner()->HasAuthority() && _host->IsCreateWheelMeshes())
	{
		_WheelMesh = _host->CreateStaticMeshComponent(_SocketName, _SocketName);
		_WheelMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		UStaticMesh* mesh = ICarClentDatas::Instance()->GetEquipmentItemDescr(_SocketName);
		_WheelMesh->SetStaticMesh(mesh);
	}*/
}


// Called every frame
void UWheelComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool b = GetOwner()->HasAuthority();

	FHitResult Hit;
	FCollisionQueryParams Params;
	Params.bReturnPhysicalMaterial = true;
	Params.AddIgnoredActor(GetOwner());

	//FTransform atr = GetOwner()->GetTransform();
	FVector Start = GetComponentTransform().GetLocation();// atr.TransformPosition(_BaseTransform.GetLocation());
	FVector End = Start - 100 * FVector::UpVector;

	bool bIsHit = GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECollisionChannel::ECC_Visibility, Params);
	DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 5.f, ECC_WorldStatic, 1.f);
	if (bIsHit)
	{
	}
}

