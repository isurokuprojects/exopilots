// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class ICarComponentOwner
{

public:	
	virtual ~ICarComponentOwner() {}

	virtual class UStaticMeshComponent* CreateStaticMeshComponent(const FName& InName, FName InSocketName = NAME_None) = 0;
	virtual class UStaticMeshComponent* CreateStaticMeshComponentOnCtor(const FName& InName, FName InSocketName = NAME_None) = 0;

	virtual FTransform GetSocketTransform(FName InSocketName, ERelativeTransformSpace TransformSpace = ERelativeTransformSpace::RTS_Actor) const = 0;
};
