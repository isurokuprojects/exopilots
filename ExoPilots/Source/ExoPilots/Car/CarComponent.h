// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "ICarComponentOwner.h"
#include "CarComponent.generated.h"

class UWheelComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EXOPILOTS_API UCarComponent : public UBoxComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCarComponent();

	void Init(ICarComponentOwner* InCarHost);

	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UStaticMeshComponent* CreateStaticMeshComponent(const FName& InName, FName InSocketName = NAME_None);
	bool IsCreateWheelMeshes() const { return CreateWheelMeshes; }

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

private:
	ICarComponentOwner* _host;

	bool _autority;

	UPROPERTY()
	TArray<UWheelComponent*>  _Wheels;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	UWheelComponent* WheelLF;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	UWheelComponent* WheelRF;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	UWheelComponent* WheelLB;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	UWheelComponent* WheelRB;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	//TArray<FName>  WheelSokets;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	bool  CreateWheelMeshes;

	//* left and right shift */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	float  WheelSocketYShift;

	//* vertical shift */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	float  WheelSocketZShift;

	//* Calm spring length */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	float  RestLength;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	float  WheelRadius;
};
