// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "../GGP/EPlace.h"
#include "WheelComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogWheelComponent, Log, All);

class UCarComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EXOPILOTS_API UWheelComponent : public UBoxComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWheelComponent();

	void Init(UCarComponent* InCarHost, EPlace InPlace);

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	//create only client
	UPROPERTY()
	UStaticMeshComponent* _WheelMesh;

	UPROPERTY(Replicated)
	UCarComponent* _host;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Car Property", meta = (AllowPrivateAccess = "true"))
	TEnumAsByte<EPlace> _Place;

	/*UFUNCTION()
	void OnRepSocketName();

	UPROPERTY(ReplicatedUsing = OnRepSocketName)*/
	UPROPERTY(Replicated)
	FName _SocketName;

	UPROPERTY(Replicated)
	FTransform _BaseTransform;
};
