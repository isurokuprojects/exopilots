// Fill out your copyright notice in the Description page of Project Settings.


#include "CarComponent.h"
#include "WheelComponent.h"

#include "../StaticData/ICarClentDatas.h"
#include "../GGP/EPlace.h"

#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UCarComponent::UCarComponent(): _autority(false)
{
	UWheelComponent* w = CreateDefaultSubobject<UWheelComponent>(TEXT("WheelCompLF"));
	w->SetupAttachment(this);
	w->Init(this, EPlace(EPlaceLeft | EPlaceFront));
	_Wheels.Add(w);
	WheelLF = w;

	w = CreateDefaultSubobject<UWheelComponent>(TEXT("WheelCompRF"));
	w->SetupAttachment(this);
	w->Init(this, EPlace(EPlaceRight | EPlaceFront));
	_Wheels.Add(w);
	WheelRF = w;

	w = CreateDefaultSubobject<UWheelComponent>(TEXT("WheelCompLB"));
	w->SetupAttachment(this);
	w->Init(this, EPlace(EPlaceLeft | EPlaceBack));
	_Wheels.Add(w);
	WheelLB = w;

	w = CreateDefaultSubobject<UWheelComponent>(TEXT("WheelCompRB"));
	w->SetupAttachment(this);
	w->Init(this, EPlace(EPlaceRight | EPlaceBack));
	_Wheels.Add(w);
	WheelRB = w;

	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
}

void UCarComponent::Init(ICarComponentOwner* InCarHost)
{
	_host = InCarHost;
}

//void UCarComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//	// Here we list the variables we want to replicate + a condition if wanted
//	//DOREPLIFETIME(UCarComponent, _Wheels);
//}

bool UCarComponent::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	/*for (int i = 0; i < _Wheels.Num(); i++)
	{
		WroteSomething |= Channel->ReplicateSubobject(_Wheels[i], *Bunch, *RepFlags);
	}*/

	return WroteSomething;
}

// Called when the game starts
void UCarComponent::BeginPlay()
{
	Super::BeginPlay();

	/*if (GetOwner()->HasAuthority())
	{
		_autority = true;
		for (const FName& name : WheelSokets)
		{
			FTransform tr = _host->GetSocketTransform(name);
			UWheelComponent* w = NewObject<UWheelComponent>(this, UWheelComponent::StaticClass(), name);
			w->SetupAttachment(this, name);
			w->RegisterComponent();
			w->Init(this, name, tr);
			_Wheels.Add(w);
		}
	}*/

	/*for (const FName& name : WheelSokets)
	{
		auto MeshComp = _host->CreateStaticMeshComponent(name, name);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		UStaticMesh* mesh = ICarClentDatas::Instance()->GetEquipmentItemDescr(name);
		MeshComp->SetStaticMesh(mesh);
	}	*/
}


// Called every frame
void UCarComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UStaticMeshComponent* UCarComponent::CreateStaticMeshComponent(const FName& InName, FName InSocketName /*= NAME_None*/)
{
	auto MeshComp = _host->CreateStaticMeshComponent(InName, InSocketName);
	return MeshComp;
}

