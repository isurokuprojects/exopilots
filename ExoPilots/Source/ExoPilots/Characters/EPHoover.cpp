// Fill out your copyright notice in the Description page of Project Settings.


#include "EPHoover.h"

#include "../Interaction/VehicleInteractableComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "../Car/CarComponent.h"

// Sets default values
AEPHoover::AEPHoover()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	SetRootComponent(BaseMesh);
	BaseMesh->SetSimulatePhysics(true);
	BaseMesh->SetEnableGravity(true);
	BaseMesh->SetMassOverrideInKg(NAME_None, 1200);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InteractableComponent = CreateDefaultSubobject<UVehicleInteractableComponent>(TEXT("InteractableComponent"));
	InteractableComponent->SetupAttachment(RootComponent);

	CarComponent = CreateDefaultSubobject<UCarComponent>(TEXT("CarComponent"));
	CarComponent->SetupAttachment(RootComponent);
	CarComponent->Init(this);

	/*_MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WheelMeshAMyHoover"));
	_MeshComp->SetupAttachment(RootComponent);*/
}

UStaticMeshComponent* AEPHoover::CreateStaticMeshComponentOnCtor(const FName& InName, FName InSocketName /*= NAME_None*/)
{
	auto MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(InName);
	MeshComp->SetupAttachment(RootComponent, InSocketName);
	return MeshComp;
}

UStaticMeshComponent* AEPHoover::CreateStaticMeshComponent(const FName& InName, FName InSocketName /*= NAME_None*/)
{
	auto MeshComp = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass(), InName);
	MeshComp->SetupAttachment(RootComponent, InSocketName);
	MeshComp->RegisterComponent();
	return MeshComp;
}

FTransform AEPHoover::GetSocketTransform(FName InSocketName, ERelativeTransformSpace InTransformSpace /*= ERelativeTransformSpace::RTS_Actor*/) const
{
	FTransform tr = BaseMesh->GetSocketTransform(InSocketName, InTransformSpace);
	return tr;
}

// Called when the game starts or when spawned
void AEPHoover::BeginPlay()
{
	Super::BeginPlay();

	FTransform tr = BaseMesh->GetSocketTransform(FName(), ERelativeTransformSpace::RTS_Actor);
	
	/*if(HasAuthority())
	{ 
		BaseMesh->SetSimulatePhysics(true);
		BaseMesh->SetEnableGravity(true);
		BaseMesh->SetMassOverrideInKg(NAME_None, 1200);
	}
	else
	{
		BaseMesh->SetSimulatePhysics(false);
	}*/

	
}

void AEPHoover::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AEPHoover::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AEPHoover::LookUpAtRate);
}

void AEPHoover::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AEPHoover::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

// Called every frame
void AEPHoover::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
