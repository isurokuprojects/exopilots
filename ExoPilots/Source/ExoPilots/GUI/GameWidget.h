// Copyright (c) 2019 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameWidget.generated.h"

class UBaseUIState;
/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCloseCallDelegate);

UCLASS()
class EXOPILOTS_API UGameWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual void Init(UBaseUIState* InState);

	virtual void BeginDestroy() override;

	//Event called when on widget hiding by UI manager
	UFUNCTION(BlueprintImplementableEvent)
	void OnCloseBlueprintEvent();

	//Event called when on widget showing by UI manager
	UFUNCTION(BlueprintImplementableEvent)
	void OnOpenBlueprintEvent();

	virtual void OnOpen();
	virtual void OnClose();

	UFUNCTION(BlueprintCallable)
	void RaiseCloseCallEvent();

	UPROPERTY(BlueprintAssignable)
	FCloseCallDelegate CloseCallHandler;

protected:

	UBaseUIState* _State;
};
