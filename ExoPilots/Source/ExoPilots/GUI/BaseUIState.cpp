// Copyright (c) 2020 GFA Games. All rights reserved.


#include "BaseUIState.h"
#include "GameWidget.h"
#include "../BaseEngineClasses/EPPlayerController.h"
#include "WidgetUtils.h"
#include "GUIManager.h"

UBaseUIState::UBaseUIState(const FName& InName): Super(InName), _Inited(false)
{
}

UBaseUIState::UBaseUIState() : UBaseUIState(NAME_None)
{
}

UGUIManager* UBaseUIState::GetOwner() const
{
	UGUIManager* Owner = Cast<UGUIManager>(GetOuter());
	ensureAlways(Owner);
	return Owner;
}

AEPPlayerController* UBaseUIState::GetPlayerController() const
{
	UGUIManager* Owner = GetOwner();
	if (Owner == nullptr)
		return nullptr;

	AEPPlayerController* PC = Owner->GetPlayerController();
	return PC;
}

EUIStateType UBaseUIState::GetUIStateType() const 
{ 
	return EUIStateType::MAX_UNDEFINED; 
}

void UBaseUIState::Init()
{
	_Inited = true;
}

void UBaseUIState::BeginDestroy()
{
	for (auto Pair : Widgets)
	{
		Pair.Value.Widget->SetVisibility(ESlateVisibility::Collapsed);
		Pair.Value.Widget->ConditionalBeginDestroy();
	}

	Widgets.Empty();

	Super::BeginDestroy();
}

bool UBaseUIState::OnExit(UFreeFSMState* InNextState, FFreeFSMSwitchData* InSwitchData)
{
	if(!Super::OnExit(InNextState, InSwitchData))
		return false;

	for (auto Pair : Widgets)
	{
		Pair.Value.Widget->SetVisibility(ESlateVisibility::Collapsed);
		Pair.Value.Widget->OnClose();
	}

	return true;
}

bool UBaseUIState::OnEnter(UFreeFSMState* InPrevState, FFreeFSMSwitchData* InSwitchData)
{
	if(!Super::OnEnter(InPrevState, InSwitchData))
		return false;

	for (auto Pair : Widgets)
	{
		if (!Pair.Value.bHideOnOpen)
		{
			Pair.Value.Widget->SetVisibility(ESlateVisibility::Visible);
			Pair.Value.Widget->OnOpen();
		}
	}

	return true;
}

UGameWidget* UBaseUIState::CreateGameWidget(const FName& InWindowName, TSubclassOf <UGameWidget> InWindowClass)
{
//	ensureAlways(InWindowClass);
	if (InWindowClass == nullptr)
		return nullptr;

	AEPPlayerController* PC = GetPlayerController();
	UGameWidget* Widget = CreateWidget<UGameWidget>(PC, InWindowClass, InWindowName);
	ensureAlways(Widget);
	if (Widget == nullptr)
		return nullptr;

	FWidgetInfo WI(Widget);
	Widgets.Add(InWindowName, Widget);

	Widget->Init(this);

	Widget->AddToViewport();
	Widget->SetVisibility(ESlateVisibility::Collapsed);

	return Widget;
}

void UBaseUIState::SetHideOnOpen(const FName& InWindowName, bool InValue)
{
	FWidgetInfo* pWI = Widgets.Find(InWindowName);
	ensure(pWI);
	if (pWI)
		pWI->bHideOnOpen = InValue;
}

void UBaseUIState::OnCloseCall(UGameWidget* InWidget)
{
	UGUIManager* Owner = GetOwner();

	if(Owner != nullptr)
		Owner->OnCloseCall(this);
}