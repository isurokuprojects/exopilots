// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseUIState.h"
#include "HumanUIState.generated.h"

class UserWidget;
class UInteractableComponent;

DECLARE_LOG_CATEGORY_EXTERN(LogHumanUIState, Log, All);

/**
 * 
 */
UCLASS()
class EXOPILOTS_API UHumanUIState : public UBaseUIState
{
	GENERATED_BODY()

public:
	virtual void Init() override;

	virtual void BeginDestroy() override;
	
private:
	UFUNCTION()
	void OnSelectedInteractableObject(UInteractableComponent* InInteractableObject);

	UGameWidget* _HumanInteractWD;
};
