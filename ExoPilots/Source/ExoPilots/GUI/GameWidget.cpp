// Copyright (c) 2019 GFA Games. All rights reserved.


#include "GameWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "BaseUIState.h"

void UGameWidget::Init(UBaseUIState* InState)
{
	_State = InState;
}

void UGameWidget::BeginDestroy()
{
	_State = nullptr;
	Super::BeginDestroy();
}

void UGameWidget::OnOpen()
{
	OnOpenBlueprintEvent();
}

void UGameWidget::OnClose()
{
	OnCloseBlueprintEvent();
}

void UGameWidget::RaiseCloseCallEvent()
{
	ensureAlways(_State);
	if (_State != nullptr)
		_State->OnCloseCall(this);

	CloseCallHandler.Broadcast();
}