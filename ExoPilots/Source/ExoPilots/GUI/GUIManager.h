// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameWidget.h"
#include "GUIManager.generated.h"

class UVehicleInteractableComponent;
class AEPPlayerController;
class UBaseUIState;
class UFreeFSM;

DECLARE_LOG_CATEGORY_EXTERN(LogGUIManager, Log, All);

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class EXOPILOTS_API UGUIManager : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UGUIManager();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widgets")
	TSubclassOf <UGameWidget> HumanInteractWidgetClass;

	void OnPossessed(APawn* P);

	//void OnItenteractAppears(UVehicleInteractableComponent* InInteractable, bool InAppears);

	AEPPlayerController* GetPlayerController() const;

	void OnCloseCall(UBaseUIState* inState);

protected:
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	void CreateStates();

	UPROPERTY()
	UFreeFSM* Fsm;

	TArray<UBaseUIState*> StatesArray;
};
