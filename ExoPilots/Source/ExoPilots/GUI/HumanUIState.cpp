// Fill out your copyright notice in the Description page of Project Settings.


#include "HumanUIState.h"
#include "Blueprint/UserWidget.h"
#include "../BaseEngineClasses/EPPlayerController.h"
#include "../Interaction/HumanPlayerInteractionComponent.h"
#include "../Characters/EPCharacter.h"
#include "../Interaction/InteractableComponent.h"
#include "GUIManager.h"

DEFINE_LOG_CATEGORY(LogHumanUIState);

FName HumanInteractName("HumanInteract");

void UHumanUIState::Init()
{
	Super::Init();

	UGUIManager* GUI_Manager = GetOwner();

	_HumanInteractWD = CreateGameWidget(HumanInteractName, GUI_Manager->HumanInteractWidgetClass);
	ensureAlways(_HumanInteractWD);

	SetHideOnOpen(HumanInteractName, true);	

	AEPPlayerController* PC = GetPlayerController();
	AEPCharacter* Char = PC->GetHumanCharacter();
	UHumanPlayerInteractionComponent* Interactor = Cast<UHumanPlayerInteractionComponent>(Char->GetComponentByClass(UHumanPlayerInteractionComponent::StaticClass()));

	Interactor->OnSelectedInteractableObjectHandlers.AddDynamic(this, &UHumanUIState::OnSelectedInteractableObject);
}

void UHumanUIState::BeginDestroy()
{
	if (IsInited())
	{
		AEPPlayerController* PC = GetPlayerController();
		if (PC)
		{
			AEPCharacter* Char = PC->GetHumanCharacter();
			if (Char)
			{
				UHumanPlayerInteractionComponent* Interactor = Cast<UHumanPlayerInteractionComponent>(Char->GetComponentByClass(UHumanPlayerInteractionComponent::StaticClass()));
				if (Interactor)
					Interactor->OnSelectedInteractableObjectHandlers.RemoveDynamic(this, &UHumanUIState::OnSelectedInteractableObject);
			}
		}
	}

	Super::BeginDestroy();
}

void UHumanUIState::OnSelectedInteractableObject(UInteractableComponent* InInteractableObject)
{
	if (InInteractableObject)
	{
		UE_LOG(LogHumanUIState, Log, TEXT("New InInteractableObject: %s"), *InInteractableObject->GetName());
		_HumanInteractWD->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		UE_LOG(LogHumanUIState, Log, TEXT("InInteractableObject none"));
		_HumanInteractWD->SetVisibility(ESlateVisibility::Collapsed);
	}
}