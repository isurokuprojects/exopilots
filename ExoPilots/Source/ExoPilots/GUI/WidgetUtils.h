// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

FName GenerateName(const FString* InWidgetName = nullptr);

bool CheckAndAddNameUnique(const FName& InWidgetName);

template <typename WidgetT = UUserWidget, typename OwnerT = UObject>
WidgetT* CreateWidgetUniqueNamed(const FName& InWidgetName, OwnerT* InOwner, TSubclassOf<UUserWidget> UserWidgetClass = WidgetT::StaticClass())
{
	FName WidgetName;
	ensureAlwaysMsgf(InWidgetName.IsValid(), TEXT("InWidgetName must be valued!"));
	if (InWidgetName.IsNone())
	{
		WidgetName = GenerateName();
	}
	else
	{
		bool checked = CheckAndAddNameUnique(InWidgetName);
		ensureAlwaysMsgf(checked, TEXT("InWidgetName %s is not unique!"), *InWidgetName.ToString());
		if (!checked)
		{
			WidgetName = GenerateName();
		}
		else
		{
			WidgetName = InWidgetName;
		}
	}

	return CreateWidget<WidgetT>(InOwner, UserWidgetClass, WidgetName);
}

template <typename WidgetT = UUserWidget, typename OwnerT = UObject>
WidgetT* CreateWidgetAutoNamed(OwnerT* InOwner, TSubclassOf<UUserWidget> UserWidgetClass = WidgetT::StaticClass())
{
	FName Name = GenerateName(nullptr);
	return CreateWidget<WidgetT>(InOwner, UserWidgetClass, Name);
}

template <typename WidgetT = UUserWidget, typename OwnerT = UObject>
WidgetT* CreateWidgetAutoNamed(OwnerT* InOwner, const FString& InWidgetName, TSubclassOf<UUserWidget> UserWidgetClass = WidgetT::StaticClass())
{
	FName Name = GenerateName(&InWidgetName);
	return CreateWidget<WidgetT>(InOwner, UserWidgetClass, Name);
}