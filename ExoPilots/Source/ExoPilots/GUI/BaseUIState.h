// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "../Support/FreeFSM.h"
#include "UIStateTypes.h"
#include "BaseUIState.generated.h"

class UGUIManager;
class UGameWidget;
class AEPPlayerController;

UCLASS()
class EXOPILOTS_API UBaseUIState : public UFreeFSMState
{
	GENERATED_BODY()

public:
	UBaseUIState(const FName& InName);
	UBaseUIState();

	virtual void Init();
	inline bool IsInited() const { return _Inited; }

	virtual bool OnExit(UFreeFSMState* InNextState, FFreeFSMSwitchData* InSwitchData) override;

	virtual bool OnEnter(UFreeFSMState* InPrevState, FFreeFSMSwitchData* InSwitchData) override;

	virtual EUIStateType GetUIStateType() const;

	void OnCloseCall(UGameWidget* InWidget);

	virtual void BeginDestroy() override;

	UGUIManager* GetOwner() const;
	AEPPlayerController* GetPlayerController() const;

protected:

	UGameWidget* CreateGameWidget(const FName& InWindowName, TSubclassOf <UGameWidget> InWidgetClass);
	void SetHideOnOpen(const FName& InWindowName, bool InValue);

private:

	bool _Inited;

	struct FWidgetInfo
	{
		UGameWidget* Widget;
		bool bHideOnOpen;

		FWidgetInfo() : Widget(nullptr), bHideOnOpen(false) {}
		FWidgetInfo(UGameWidget* InWidget) : Widget(InWidget), bHideOnOpen(false) {}
	};

	TMap<FName, FWidgetInfo> Widgets;
};