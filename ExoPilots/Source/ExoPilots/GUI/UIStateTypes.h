// Copyright (c) 2020 GFA Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include <Runtime/Engine/Classes/Engine/Blueprint.h>

UENUM(BlueprintType)
enum class EUIStateType : uint8
{
	HUMAN_HUD UMETA(DisplayName = "HUMAN_HUD"),
	/*INVENTORY UMETA(DisplayName = "INVENTORY"),
	CHAR_CREATOR UMETA(DisplayName = "CHAR_CREATOR"),
	WORLD_MAP UMETA(DisplayName = "WORLD_MAP"),
	FAST_WEAPON_CHANGE UMETA(DisplayName = "FAST_WEAPON_CHANGE"),
	DEAD UMETA(DisplayName = "Dead"),
	SHOP UMETA(DisplayName = "Shop"),
	JORNAL UMETA(DisplayName = "Jornal"),
	GAME_MENU UMETA(DisplayName = "GAME_MENU"),
	WEAPON_ATTACH UMETA(DisplayName = "WEAPON_ATTACH"),*/
	MAX_UNDEFINED UMETA(DisplayName = "MAX_UNDEFINED"),
};