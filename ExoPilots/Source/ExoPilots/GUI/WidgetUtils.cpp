// Copyright (c) 2020 GFA Games. All rights reserved.


#include "WidgetUtils.h"

FName GenerateName(const FString* InWidgetName)
{
	static uint64 counter = 0;
	counter++;

	if (InWidgetName == nullptr)
		return FName(*FString::Printf(TEXT("Widget_%d"), counter));
	FString res = FString::Printf(TEXT("%s_%d"), **InWidgetName, counter);
	return FName(*res);
}

bool CheckAndAddNameUnique(const FName& InWidgetName)
{
	static TSet<FName> UniqueNames;

	if (UniqueNames.Contains(InWidgetName))
		return false;
	UniqueNames.Add(InWidgetName);
	return true;
}