// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameWidget.h"
#include "GameWidget_HumanInteract.generated.h"



DECLARE_LOG_CATEGORY_EXTERN(LogWDHumanInteract, Log, All);

/**
 * 
 */
UCLASS()
class EXOPILOTS_API UGameWidget_HumanInteract : public UGameWidget
{
	GENERATED_BODY()
	
public:
	virtual void Init(UBaseUIState* InState) override;
	virtual void BeginDestroy() override;

	//UFUNCTION(BlueprintImplementableEvent, Category = "Inventory System")
	//void Show(const FInventoryItem2& InInventoryItem);

	//virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

private:
};
