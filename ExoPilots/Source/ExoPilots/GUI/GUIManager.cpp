// Fill out your copyright notice in the Description page of Project Settings.


#include "GUIManager.h"
#include "../BaseEngineClasses/EPPlayerController.h"
#include "HumanUIState.h"

DEFINE_LOG_CATEGORY(LogGUIManager);

UGUIManager::UGUIManager()
{
}


void UGUIManager::OnPossessed(APawn* P)
{
	AEPPlayerController* PC = GetPlayerController();
	UE_LOG(LogGUIManager, Log, TEXT("OnPossessed: PC_Role: %s; Pawn %s"), *PC->GetRoleTextInfo(), *P->GetName());

	CreateStates();
}

void UGUIManager::CreateStates()
{
	Fsm = NewObject<UFreeFSM>(this);

	StatesArray.Add(NewObject<UHumanUIState>(this));

	for (UBaseUIState* state : StatesArray)
	{
		state->Init();
	}

	if(StatesArray.Num() > 0)
		Fsm->Switch(StatesArray[0]);
}

void UGUIManager::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	for (UBaseUIState* state : StatesArray)
	{
		state->ConditionalBeginDestroy();
	}

	Super::EndPlay(EndPlayReason);
}

AEPPlayerController* UGUIManager::GetPlayerController() const
{
	AEPPlayerController* PC = Cast<AEPPlayerController>(GetOwner());
	ensureAlways(PC);
	return PC;
}

//void UGUIManager::OnItenteractAppears(UVehicleInteractableComponent* InInteractable, bool InAppears)
//{
//	/*if (_WD_Interact != nullptr)
//	{
//		ESlateVisibility Visibility = ESlateVisibility::Collapsed;
//		if(InAppears)
//			Visibility = ESlateVisibility::Visible;
//		
//		_WD_Interact->SetVisibility(Visibility);
//	}*/
//}

void UGUIManager::OnCloseCall(UBaseUIState* inState)
{

}