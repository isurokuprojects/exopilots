// Fill out your copyright notice in the Description page of Project Settings.


#include "EPPlayerController.h"
#include "../Characters/EPCharacter.h"
#include "../Support/StringUtils.h"
#include "../GUI/GUIManager.h"

DEFINE_LOG_CATEGORY(LogEPPlayerController);

AEPPlayerController::AEPPlayerController()
{
	GUIManager = CreateDefaultSubobject<UGUIManager>(TEXT("GUIManager"));
}

void AEPPlayerController::AcknowledgePossession(APawn* P)
{
	Super::AcknowledgePossession(P);

	AEPCharacter* Char = Cast<AEPCharacter>(P);
	if (Char)
	{
		if (_HumanCharacter == nullptr)
		{
			_HumanCharacter = Char;
		}
		else
		{
			ensureAlways(_HumanCharacter == Char);
		}

		Char->TryOnPossessedRaise(this, false);

		if (GetLocalRole() == ENetRole::ROLE_AutonomousProxy)
		{
			GUIManager->OnPossessed(P);
		}
	}
}

FString AEPPlayerController::GetRoleTextInfo() const
{
	ENetRole lr = GetLocalRole();
	ENetRole rr = GetRemoteRole();
	return FString::Printf(TEXT("%s[%s]"), *RoleToString(lr), *RoleToString(rr));
}

//void AMyPlayerController::OnItenteractAppears(UVehicleInteractableComponent* InInteractable, bool InAppears)
//{
//	GUIManager->OnItenteractAppears(InInteractable, InAppears);
//}