// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "EPPlayerController.generated.h"

class AEPCharacter;
class UGUIManager;
class UVehicleInteractableComponent;

DECLARE_LOG_CATEGORY_EXTERN(LogEPPlayerController, Log, All);

/**
 * 
 */
UCLASS()
class EXOPILOTS_API AEPPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	AEPPlayerController();

	virtual void AcknowledgePossession(APawn* P) override;

	FString GetRoleTextInfo() const;

	inline AEPCharacter* GetHumanCharacter() const { return _HumanCharacter; }

	//void OnItenteractAppears(UVehicleInteractableComponent* InInteractable, bool InAppears);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GUI Manager", meta = (AllowPrivateAccess = "true"))
		UGUIManager* GUIManager;

	UPROPERTY()
	AEPCharacter* _HumanCharacter;
};
