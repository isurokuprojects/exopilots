// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EPGameMode.generated.h"

UCLASS(minimalapi)
class AEPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEPGameMode();
};



