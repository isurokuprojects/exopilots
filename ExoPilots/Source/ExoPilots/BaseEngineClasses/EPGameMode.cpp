// Copyright Epic Games, Inc. All Rights Reserved.

#include "EPGameMode.h"
#include "EPPlayerController.h"
#include "../Characters/EPCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEPGameMode::AEPGameMode()
{
	static ConstructorHelpers::FClassFinder<AEPPlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/EPPlayerController"));
	//PlayerControllerClass = AMyPlayerController::StaticClass();
	if (PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/EPCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	//GameStateClass = AMyGameState::StaticClass();
}
