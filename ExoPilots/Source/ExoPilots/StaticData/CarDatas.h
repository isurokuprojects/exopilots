// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ICarClentDatas.h"
#include "WheelMeshesTableRow.h"

#include "CarDatas.generated.h"

/**
 * 
 */
UCLASS()
class EXOPILOTS_API UCarDatas : public UObject, public ICarClentDatas
{
	GENERATED_BODY()
	
public: 

	UCarDatas();

	virtual bool IsSupportedForNetworking() const override { return true; }
	virtual void GetLifetimeReplicatedProps(TArray< class FLifetimeProperty >& OutLifetimeProps) const override;

	UFUNCTION()
	void LoadServerDatas();

	UFUNCTION()
	void LoadClientDatas();

	virtual UStaticMesh* GetEquipmentItemDescr(const FName& InName);

private:

	UPROPERTY()
	TMap< FName, UStaticMesh* > _WheelMeshesMap;
};
