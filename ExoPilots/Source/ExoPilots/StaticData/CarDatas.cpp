// Fill out your copyright notice in the Description page of Project Settings.


#include "CarDatas.h"
#include "../Support/DataTableUtils.h"
#include "DataTablePathes.h"

ICarClentDatas* ICarClentDatas::_instance;

UCarDatas::UCarDatas()
{
	ICarClentDatas::_instance = this;
}

void UCarDatas::GetLifetimeReplicatedProps(TArray< class FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	/*DOREPLIFETIME(UEquipmentStaticData, Test);
	DOREPLIFETIME(UEquipmentStaticData, EquipmentItemParamDescrs);*/
}

void UCarDatas::LoadServerDatas()
{
}

void UCarDatas::LoadClientDatas()
{
	UDataTable* Table = LoadTable(CarWheelsMeshesTablePath);
	if (Table == nullptr)
		return;
	for (TMap<FName, uint8*>::TConstIterator RowMapIter(Table->GetRowMap().CreateConstIterator()); RowMapIter; ++RowMapIter)
	{
		FWheelMeshesTableRow* Row = reinterpret_cast<FWheelMeshesTableRow*>(RowMapIter.Value());
		_WheelMeshesMap.Add(RowMapIter.Key(), Row->WheelMesh);
	}
}

UStaticMesh* UCarDatas::GetEquipmentItemDescr(const FName& InName)
{
	if (_WheelMeshesMap.Num() == 0)
		return nullptr;

	UStaticMesh** rec = _WheelMeshesMap.Find(InName);
	if (rec == nullptr)
		return _WheelMeshesMap.begin()->Value;
	return *rec;
}