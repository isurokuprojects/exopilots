// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "WheelMeshesTableRow.generated.h"

USTRUCT(BlueprintType)
struct FWheelMeshesTableRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UStaticMesh* WheelMesh;
};
