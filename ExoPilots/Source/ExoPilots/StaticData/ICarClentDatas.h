// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMesh.h"

class ICarClentDatas
{
public:
	virtual ~ICarClentDatas() {}

	static ICarClentDatas* Instance() { return _instance; }

	virtual UStaticMesh* GetEquipmentItemDescr(const FName& InName) = 0;

protected:
	static ICarClentDatas* _instance;
};